.PHONY:  default up down ps build logs

default:
	echo "make [up | down | ps | build]"
	docker-compose ps
up:
	docker-compose up -d

down:
	docker-compose down
ps:
	docker-compose ps

log:
	docker-compose logs

build:
	docker-compose build


